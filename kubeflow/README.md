# mpi_learn deployment infrastructure

Deploy [mpi_learn](https://github.com/svalleco/mpi_learn) on a GPU Kubernetes cluster with Kubeflow

## Instructions

1. Create a Magnum Kubernetes cluster with a GPU flavor
2. Deploy the [Nvidia GPU device plugin](https://gitlab.cern.ch/kosamara/nvidia-system-container/blob/coreos/daemonsets/nvidia-gpu-device-plugin.yaml)
3. Deploy Kubeflow, following the instructions in [Kubeflow's quick start guide](https://www.kubeflow.org/docs/started/getting-started/)
4. Deploy the Kubeflow component `mpi_operator`, which is not included by default
  - In the kubeflow ksonnet app, generate an `mpi_operator` component. Specify the number of GPUs per node!
    `ks generate mpi-operator mpi-operator --gpusPerNode=<...>`
  - `ks apply <env> -c mpi-operator`
5. Deploy the [app resources](kubeflow/resources)
  - The [ksonnet app](kubeflow/svalleco-mpi_learn-ks/) is meant to provide an alternative deployment option,
    but it doesn't yet work. It needs to provide a multiline string as argument to the train command.

## Generic cloud (no managed Kubernetes)

If working on a generic cloud without managed Kubernetes deployments, instead of steps 1-2 above
follow [these instructions](kubernetes/bootstrap-exoscale/README.md).
Then, continue from step 3.
