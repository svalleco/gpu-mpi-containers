#!/bin/sh

yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum install -y 'docker-ce-18.06.1.ce'
mkdir "/etc/docker"
cat > "/etc/docker/daemon.json" <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
mkdir -p "/etc/systemd/system/docker.service.d"

systemctl enable docker
systemctl daemon-reload
systemctl start docker
